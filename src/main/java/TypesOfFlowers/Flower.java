package TypesOfFlowers;


import Lukasz.Michalak.FlowerFactory;

public abstract class Flower  {
    private String name;
    private String colour;

    private int quantity;
    private double price;

    public Flower(String name, String colour) {
        this.name = name;
        this.colour = colour;
        this.price = 0;
    }

    public Flower(int quantity){
        this.quantity = quantity;
        this.price = 0;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
