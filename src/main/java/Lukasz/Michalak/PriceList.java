package Lukasz.Michalak;


import java.util.HashMap;
import java.util.Map;

public class PriceList {

    private static PriceList instance = null;

    protected Map<String,Double> flowerPrices = new HashMap<String, Double>();

    private PriceList(){
    }

    public static PriceList getInstance(){
        if (instance==null){
            instance=new PriceList();
        }
        return instance;
    }

    public Map<String, Double> getFlowerPrices() {
        return flowerPrices;
    }

    public void set(String name, double price ){
        flowerPrices.put(name,price);
    }


}
