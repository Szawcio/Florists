package Lukasz.Michalak;


import TypesOfFlowers.Flower;

import java.util.HashMap;
import java.util.Map;

public class ShoppingCart {

    private Customer owner;
    private Map<Flower, Integer> customerCartMap;

    public ShoppingCart() {
        customerCartMap = new HashMap<>();
    }

    public Customer getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Shoppingcart owner: " ).append(owner).append("/n");
        for (Flower flower : customerCartMap.keySet()) {
            sb.append(flower.getName()).append(", ").append("colour: ").append(flower.getColour()).append(", quantity ").append(flower.getQuantity()).append(", price ").append(flower.getPrice()).append("/n");

        }
        return sb.toString();
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }

    public Map<Flower, Integer> getCustomerCartMap() {
        return customerCartMap;
    }

    public void setCustomerCartMap(Map<Flower, Integer> customerCartMap) {
        this.customerCartMap = customerCartMap;
    }
}
