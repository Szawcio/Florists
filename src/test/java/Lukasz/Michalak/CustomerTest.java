package Lukasz.Michalak;


import TypesOfFlowers.Freesia;
import TypesOfFlowers.Lilac;
import TypesOfFlowers.Peony;
import TypesOfFlowers.Rose;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;

public class CustomerTest {

    private Customer customer;

    @Before
    public void setup() {
        PriceList pl = PriceList.getInstance();
        pl.set("rose", 10);
        pl.set("lilac", 12);
        pl.set("peony", 8);
        customer = new Customer("lukasz", 120);

    }

    @Test
    public void testGet1() {
        customer.get(FlowerFactory.createSomeFlowers("Rose", 2));
        boolean result = customer.getShoppingCart().getCustomerCartMap().containsValue(2);
        assertThat(result).isTrue();
    }

    @Test
    public void testGet2() {
        customer.get(FlowerFactory.createSomeFlowers("Rose", 2));
        customer.get(FlowerFactory.createSomeFlowers("Lilac", 5));
        boolean result = customer.getShoppingCart().getCustomerCartMap().containsValue(5);
        assertThat(result).isTrue();

    }

    @Test
    public void testPay() {
        customer.get(FlowerFactory.createSomeFlowers("Rose", 5));
        customer.get(FlowerFactory.createSomeFlowers("Lilac", 5));
        //customer.get(new Peony(100));   <- here, checked if loop will stop if customer dont have enought money
        customer.pay();
        assertThat(customer.getCash()).isEqualTo(10);

    }

    @Test
    public void testPack() {
        customer.get(FlowerFactory.createSomeFlowers("Rose", 5));
        customer.pay();
        customer.pack();
        boolean result = customer.getShoppingCart().getCustomerCartMap().isEmpty();
        assertThat(result).isTrue();


    }

    @Test
    public void testValueOf() {
        customer.get(FlowerFactory.createSomeFlowers("Rose", 20));
        customer.get(FlowerFactory.createSomeFlowers("Lilac", 5));
        customer.get(FlowerFactory.createSomeFlowers("Peony", 10));
        customer.pack();
        double result = customer.valueOf("red");
        assertThat(result).isEqualTo(280);


    }

}
