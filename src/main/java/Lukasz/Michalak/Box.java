package Lukasz.Michalak;


import TypesOfFlowers.Flower;

import java.util.HashMap;
import java.util.Map;

public class Box {

    private Customer owner;
    private Map<Flower,Integer> customerBox;

    public Box() {

        customerBox = new HashMap<Flower, Integer>();
    }

    public Map<Flower, Integer> getCustomerBox() {
        return customerBox;
    }

    public void setCustomerBox(Map<Flower, Integer> customerBox) {
        this.customerBox = customerBox;
    }
}
