package Lukasz.Michalak;


import TypesOfFlowers.*;

public class FlowerFactory {

    public static Flower createSomeFlowers(String typ, int quantity){
        switch (typ) {
            case "Rose":
                Rose rose = new Rose(quantity);
                rose.setName("rose");
                rose.setColour("red");
                return rose;

            case "Lilac":
                Lilac lilac = new Lilac(quantity);
                lilac.setName("lilac");
                lilac.setColour("white");
                return lilac;

            case "Peony":
                Peony peony = new Peony(quantity);
                peony.setName("peony");
                peony.setColour("red");
                return peony;

            case "Freesia":
                Freesia freesia = new Freesia(quantity);
                freesia.setName("freesia");
                freesia.setColour("yellow");
                return freesia;
            default:
                return null;
        }
    }

    public static Flower createSomeFlowers(String typ) {
        return createSomeFlowers(typ, 1);
    }


}
