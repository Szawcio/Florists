package Lukasz.Michalak;


import TypesOfFlowers.*;

import java.util.Iterator;
import java.util.Map;

public class Customer {
    String name;
    double cash;
    ShoppingCart shoppingCart;
    Box box;


    public Customer(String name, double cash) {
        this.name = name;
        this.cash = cash;
        shoppingCart = new ShoppingCart();
        box = new Box();
    }


    public void get(Flower flower) {
        for (Map.Entry<String, Double> entry : PriceList.getInstance().getFlowerPrices().entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();

            if (flower.getName().equals(key)) {
                flower.setPrice(value);
                getShoppingCart().getCustomerCartMap().put(flower, flower.getQuantity());
            } else {
                getShoppingCart().getCustomerCartMap().put(flower, flower.getQuantity());
            }
        }


    }

    public void pay() {
        Iterator<Map.Entry<Flower, Integer>> iterator = getShoppingCart().getCustomerCartMap().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Flower, Integer> entry = iterator.next();
            if (entry.getKey().getPrice() == 0) {
                iterator.remove();
            } else {
                if (getCash() - entry.getKey().getPrice() * entry.getValue() > 0) {
                    setCash(getCash() - entry.getKey().getPrice() * entry.getValue());

                } else {
                    break;
                }
            }
        }


    }

    public void pack(){
        Iterator<Map.Entry<Flower,Integer>> iter = getShoppingCart().getCustomerCartMap().entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Flower, Integer> entry = iter.next();
            getBox().getCustomerBox().put(entry.getKey(),entry.getValue());
            iter.remove();

        }
    }

    public double valueOf(String colour){
        double sum=0;
        for (Map.Entry<Flower,Integer> entry:getBox().getCustomerBox().entrySet() ) {
            Flower flower = entry.getKey();
            Integer value = entry.getValue();
            if (colour.equals(flower.getColour())){
                sum = sum + flower.getPrice()*value;
            }
        }
        return sum;
    }



    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCash() {
        return cash;
    }


    public void setCash(double cash) {
        this.cash = cash;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }
}
